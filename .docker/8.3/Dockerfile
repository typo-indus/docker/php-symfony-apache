FROM composer:2.8.5 as composer

FROM php:8.3.17-apache-bookworm AS base
# --------------------------------------------- Mets à jour les dépendances --------------------------------------------

RUN apt-get update && \
    apt-get upgrade -y --no-install-recommends \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ------------------------------------------ Installe les packages nécessaires -----------------------------------------

RUN apt-get update && \
    apt-get install -y --no-install-recommends wget libicu-dev libzip-dev zip unzip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# --------------------------------------------- Installe les extensions PHP --------------------------------------------

RUN docker-php-ext-configure zip && docker-php-ext-install opcache intl zip \
    && a2enmod rewrite \
    && pecl install apcu \
    && docker-php-ext-enable apcu

# ---------------------------------------------------- Permissions -----------------------------------------------------

# - Configure l'utilisateur 1000 pour la compatibilité avec docker
RUN userdel -f www-data \
    && if getent group www-data ; then groupdel www-data; fi \
    && groupadd -g 1000 www-data \
    && useradd -l -u 1000 -g www-data www-data \
    && usermod -aG sudo www-data \
    && install -d -m 0755 -o www-data -g www-data /home/www-data \
    && chown --changes --silent --no-dereference --recursive \
          --from=33:33 1000:1000 \
        /home/www-data \
        /var/www

# --------------------------------------------------- Configuration ----------------------------------------------------

# Timezone
ENV TZ=Europe/Paris
RUN echo "date.timezone = Europe/Paris" > /usr/local/etc/php/conf.d/timezone.ini \
    && mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Ajout de la configuration de base PHP
COPY ./conf/php.ini $PHP_INI_DIR/conf.d/app.ini

# Ajout de la configuration de PHP-APACHE
COPY ./errors /errors
COPY ./conf/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY ./conf/apache-prod.conf /etc/apache2/conf-available/z-app.conf

RUN a2enconf z-app

# ------------------------------------------------------ Composer ------------------------------------------------------

RUN mkdir -p /var/www/.composer && chown -R www-data:www-data /var/www/.composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# -------------------------------------------------------- APP ---------------------------------------------------------

RUN mkdir /app && chown -R www-data:www-data /app
WORKDIR /app


FROM base AS psql

# ----------------------------------------------------- PostgreSQL -----------------------------------------------------

RUN apt-get update && \
	apt-get install -y --no-install-recommends libpq-dev \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-install pdo_pgsql


FROM psql AS psql-debug

# ------------------------------------------------------- XDEBUG -------------------------------------------------------

RUN pecl install xdebug && docker-php-ext-enable xdebug
