# 🐳 Image Docker Apache PHP pour Symfony

[![pipeline status](https://gitlab.com/typo-indus/docker/php-symfony-apache/badges/main/pipeline.svg)](https://gitlab.com/typo-indus/docker/php-symfony-apache/-/commits/main)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/48996895)](https://gitlab.com/typo-indus/docker/php-symfony-apache/-/commits/main)
[![Latest Release](https://gitlab.com/typo-indus/docker/php-symfony-apache/-/badges/release.svg)](https://gitlab.com/typo-indus/docker/php-symfony-apache/-/releases)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/typo-indus/docker/php-symfony-apache/-/blob/main/LICENSE)

Cette image Docker vous permet de lancer un projet web symfony.

## 🚀 Utilisation

1. Récupérez l'image Docker à partir du registre Docker :

```bash
docker pull registry.gitlab.com/typo-indus/docker/php-symfony-apache/8.2:<tag>
docker pull registry.gitlab.com/typo-indus/docker/php-symfony-apache/8.3:<tag>
docker pull registry.gitlab.com/typo-indus/docker/php-symfony-apache/8.4:<tag>
```

| tag        | description                                                            |
|------------|------------------------------------------------------------------------|
| main       | Contient le minimum.                                                   |
| psql       | Contient le mimimum + les dépendances pour psql                        |
| psql-debug | Contient les dépendances pour psql et xdebug. A n'utiliser qu'en dev.  |

## 👥 Comment Contribuer

Pas de contribution pour le moment. Contactez-moi via les issues.

## 📄 License

Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.
